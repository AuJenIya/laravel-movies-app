
## Projet Laravel movies

Suite à plusieurs semaines de pratique sur le framework Symfony pendant ma formation CDA à l'ENI Informatique, j'ai souhaité
développer un projet avec le framework Laravel afin d'étendre mes connaissances.

Cela me permet également de faire ma propre opinion sur l'utilisation de ces deux frameworks afin de m'orienter sur celui qui me convient le mieux.

Ce projet se base sur la série de tutoriels vidéos d'André Madarang et utilise la très bonne API : The Movie Database.

L'ensemble des technologies ou notions rencontrées dans ce projet sont :

- Laravel 8
- Consommation d'API
- Livewire
- Laravel view models
- Tests fonctionnels
- Tailwind CSS
- Alpine JS


