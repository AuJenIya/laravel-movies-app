<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

class MovieCard extends Component
{
    public $movie;

    /**
     * Create a new component instance.
     *
     * @param $movie
     */
    public function __construct($movie)
    {
        $this->movie = $movie;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.movie-card');
    }
}
